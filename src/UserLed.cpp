/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/

#include "UserLed.hpp"
#include "PinsConfiguration.hpp"

UserLed* UserLed::instance_ = nullptr;

UserLed::UserLed() : state_(State::CLEAR),
            led_(CRGB::Black),
            semaphore_(xSemaphoreCreateMutex()),
            newStateQueue_(xQueueCreate(10, sizeof(NewState)))
{
    instance_ = this;
}

void UserLed::begin(){
    FastLED.addLeds<WS2812B, RGB_LED_PIN, GRB>(&led_, 1);
    FastLED.setBrightness(128);
    FastLED.clear();
}

void UserLed::loop(){
    NewState newState;
    if(xQueueReceive( newStateQueue_, &newState, pdMS_TO_TICKS(100)) == pdPASS ){
        xSemaphoreTake(semaphore_, portMAX_DELAY);
        state_ = newState.state;
        xSemaphoreGive(semaphore_);

        switch(newState.state){
            case State::CLEAR:
                if(led_ != CRGB::Black){
                    FastLED.showColor(CRGB::Black);
                }
                break;
            case State::PROBE_DEVICES:
                led_ = CRGB::Yellow;
                FastLED.show();
                break;
            case State::SHOW_DEV_COUNT:
                for(uint32_t i=0;i<newState.payload;++i){
                    led_ = CRGB::Blue;
                    FastLED.show();
                    delay(250);
                    led_ = CRGB::Black;
                    FastLED.show();
                    delay(250);
                }
                break;
            case State::SIGMA_OK:
                led_ = CRGB::Green;
                FastLED.show();
                delay(500);
                led_ = CRGB::Black;
                FastLED.show();
                break;
            case State::SIGMA_NOK:
                led_ = CRGB::Red;
                FastLED.show();
                delay(500);
                led_ = CRGB::Black;
                FastLED.show();
                break;
            case State::ERROR:
                led_ = CRGB::Red;
                FastLED.show();
                break;
            case State::WARNING:
                led_ = CRGB::Orange;
                FastLED.show();
                break;
            case State::SAVING_PARAMS:
                led_ = CRGB::LightCyan;
                FastLED.show();
                break;
            case State::CUSTOM:
                led_ = newState.payload;
                FastLED.show();
                break;
            case State::PRE_LIGHT:
                led_ = CRGB::Gray;
                FastLED.show();
                break;
            case State::FLASH:
                led_ = CRGB::White;
                FastLED.show();
                break;
        }

    }else if(state_ == State::ERROR){
        if(led_ == CRGB::Red){
            led_ = CRGB::Black;
            FastLED.show();
        }else{
            led_ = CRGB::Red;
            FastLED.show();
        }
    }else if(state_ == State::WARNING){
        if(led_ == CRGB::Orange){
            led_ = CRGB::Black;
            FastLED.show();
        }else{
            led_ = CRGB::Orange;
            FastLED.show();
        }
    }else if(state_ == State::FLASH){
        if(led_ == CRGB::White){
            led_ = CRGB::Black;
            FastLED.show();
        }
    }
}

void UserLed::startProbe()
{
    setNewState(State::PROBE_DEVICES);
}

void UserLed::clear()
{
    setNewState(State::CLEAR);
}

void UserLed::setDevCount(int count)
{
    setNewState(State::SHOW_DEV_COUNT, count);
}

void UserLed::sigmaOk()
{
    setNewState(State::SIGMA_OK);
}

void UserLed::sigmaNOk()
{
    setNewState(State::SIGMA_NOK);
}

void UserLed::error()
{
    //Don't send if already in error
    if(getState() != State::ERROR){
        setNewState(State::ERROR);
    }
}

void UserLed::warning()
{
    //Don't send if already in warning
    if(getState() != State::WARNING){
        setNewState(State::WARNING);
    }
}
void UserLed::clearError()
{
    //Don't send if no errors
    if((getState() == State::ERROR) || (getState() == State::WARNING)){
        setNewState(State::CLEAR);
    }
}

void UserLed::savingParameters()
{
    setNewState(State::SAVING_PARAMS);
}

void UserLed::setNewState(State newState, uint32_t payload)
{
    NewState state = {newState, payload};
    xQueueSend( /* The handle of the queue. */
            newStateQueue_,
            ( void * ) &state,
            ( TickType_t ) 0 );
}

UserLed::State UserLed::getState()
{
    State ret;
    xSemaphoreTake(semaphore_, portMAX_DELAY);
    ret = state_;
    xSemaphoreGive(semaphore_);
    return ret;
}

void UserLed::customColor(uint8_t r, uint8_t g, uint8_t b)
{
    CRGB customColor;
    customColor.r = r;
    customColor.g = g;
    customColor.b = b;
    setNewState(State::CUSTOM, (uint32_t)customColor);
}

void UserLed::preLight()
{
    setNewState(State::PRE_LIGHT);
}

void UserLed::photoTrig()
{
    setNewState(State::FLASH);
}
