#include <Arduino.h>
#include "PhotoTaker.hpp"
#include "PinsConfiguration.hpp"
#include "Configuration.hpp"
#include "ZerotronicDevices.hpp"
#include "UserLed.hpp"

void PhotoTaker::begin()
{
    pinMode(PHOTO_TRIG_PIN, OUTPUT);
    pinMode(FLASH_TRIG_PIN, OUTPUT);
    pinMode(FLASH_INT_PIN, OUTPUT);
}

Error PhotoTaker::full(double& actualSigma, double& maxSigma, int count, int pauseSecs)
{
    Error ret = Error::NO_ERROR;
    ret = ZerotronicDevices::getInstance()->getMaximumSigma(actualSigma);
    maxSigma = Configuration::getInstance()->getSigmaLimit().getValue();
    if(actualSigma > maxSigma){
        ret = Error::UNSTABLE;
        UserLed::getInstance()->sigmaNOk();
    }else{
        photo(count, pauseSecs);
        UserLed::getInstance()->sigmaOk();
    }
    return ret;
}

Error PhotoTaker::photo(int count, int pauseSecs)
{
    //Sets light intensity
    int intensity = (int)(Configuration::getInstance()->getFlashPower().getValue()*255.0/100.0);
    for(int i=0;i<count;++i){
        unsigned long startTime = millis();
        analogWrite(FLASH_INT_PIN, intensity);
        digitalWrite(FLASH_TRIG_PIN, 1);
        UserLed::getInstance()->preLight();
        delay(Configuration::getInstance()->getPreLight().getValue());
        UserLed::getInstance()->photoTrig();
        digitalWrite(PHOTO_TRIG_PIN, 1);
        delay(100);
        digitalWrite(PHOTO_TRIG_PIN, 0);
        digitalWrite(FLASH_TRIG_PIN, 0);
        if(i<(count-1)){
            unsigned long elapsed = millis() - startTime;
            unsigned long toWait = (pauseSecs*1000) - elapsed;
            delay(toWait);
        }
    }
    return Error::NO_ERROR;
}
