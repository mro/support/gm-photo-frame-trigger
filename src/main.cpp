/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/
#include <map>
#include <Arduino.h>
#include "Zerotronic.hpp"
#include "ZerotronicDevices.hpp"
#include "UserLed.hpp"
#include "Configuration.hpp"
#include "CommandHandler.hpp"
#include "PinsConfiguration.hpp"
#include "PhotoTaker.hpp"

static Zerotronic zeroTronic(Serial1);
static ZerotronicDevices zeroTronicDevices(zeroTronic);
static UserLed userLed;
static Configuration configuration;
static CommandHandler cmdHandler(Serial);

/**
 * FreeRTOS task to read sensors
*/
void readTask(void *params) {
  while(true){
    zeroTronicDevices.loop();
    delay(1);
  }
}

/**
 * FreeRTOS task to manage LED
*/
void ledTask(void *params){
  while(true){
    userLed.loop();
    delay(10);
  }
}

void setup() {
  Serial.begin(115200);
  Serial1.begin(57600, SERIAL_7N2, UART2_RX, UART2_TX);
  userLed.begin();
  PhotoTaker::begin();
  //Starts a task for LED
  xTaskCreate(ledTask, "ledTask", 2048, NULL, 1, NULL);
  //Load configuration file
  configuration.begin();

  zeroTronic.setCommunicationTimeout(Configuration::getInstance()->getComTimeOut().getValue());
  //Probe devices
  userLed.startProbe();
  int found = zeroTronicDevices.probeDevices();
  userLed.clear();
  if(found > 0){
    userLed.setDevCount(found);
    for(ZerotronicDevices::DevicesVector::iterator it = zeroTronicDevices.getDevices().begin();
      it != zeroTronicDevices.getDevices().end(); ++it){
        double temperature = 0.0;
        (*it)->readTemperature(temperature);
    }
  }else{
    userLed.error();
  }
  cmdHandler.begin();
  //Configure processes
  zeroTronicDevices.setReadFrequency(configuration.getClinoFrequency().getValue());
  zeroTronicDevices.setReadSamples(configuration.getClinoAcquisitions().getValue());
  //Create a task to read sensors
  xTaskCreate(readTask, "readTask", 2048, NULL, 2, NULL);
}

unsigned long lastCall = 0;
void loop() {
  //Command handler
  cmdHandler.loop();
  //Delayed configuration saving
  configuration.loop();
  delay(1);
}
