/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/

#include "ZerotronicDevice.hpp"
#include "Zerotronic.hpp"

ZerotronicDevice::ZerotronicDevice(uint8_t address, Zerotronic& comm) : 
    address_(address), comm_(comm), semaphore_(xSemaphoreCreateMutex()),
    sampleIndex_(0), sampleCount_(0), nbSamples_(10),
    actualSigma_(0.0), actualMean_(0.0),
    samples_{0}, tempCountRef_(0), temperatureRef_(std::nanf("")),
    prevSeqNumber_(0xFF), lastError_(Error::NO_ERROR)
{
}

void ZerotronicDevice::computeStatistics()
{
    //Compute means of population
    double mean, sum = 0.0;
    for(int i=0;i<sampleCount_;++i){
        sum += samples_[i];
    }
    mean = sum / (double)sampleCount_;
    double distanceSum = 0.0;
    for(int i=0;i<sampleCount_;++i){
        double distance = (samples_[i] - mean);
        distanceSum += pow(distance, 2);
    }
    double distance = distanceSum / (double)sampleCount_;
    double newSigma = sqrt(distance);
    if(semaphore_){
        xSemaphoreTake(semaphore_, portMAX_DELAY);
    }
    actualMean_ = mean;
    actualSigma_ = newSigma;
    if(semaphore_){
        xSemaphoreGive(semaphore_);
    }
}

Error ZerotronicDevice::readSensor(double& angle)
{   
    Error ret;
    uint8_t seqNumber;
    ret = comm_.readAngle(address_, angle, seqNumber);
    if(ret == 0){
        if((angle != 7.0) && (angle != -7.0)){
            samples_[sampleIndex_] = angle;
            if(sampleCount_ < nbSamples_){
                ++sampleCount_;
            }
            if(++sampleIndex_ >= nbSamples_){
                //End of buffer
                sampleIndex_ = 0;
            }
            computeStatistics();
        }else{
            return Error::OUT_OF_RANGE;
        }
    }
    lastError_ = ret;
    return ret;
}

Error ZerotronicDevice::setNbSamples(int nbSamples)
{
    if((nbSamples > 0) && (nbSamples <= MAX_BUFFER_SIZE)){
        nbSamples_ = nbSamples;
        return Error::OUT_OF_RANGE;
    }
    return Error();
}

Error ZerotronicDevice::readTemperature(double& temperature)
{
    Error ret;
    //Read reference from EEPROM if not done already
    if(std::isnan(temperatureRef_)){
        ret = comm_.readReferenceTemperatureCelsius(address_, temperatureRef_);
        if(ret)
            return ret;
        ret = comm_.readReferenceTemperatureCount(address_, tempCountRef_);
        if(ret){
            temperatureRef_ = std::nanf("");
            return ret;
        }
    }
    uint32_t tempCount;
    ret = comm_.readTemperatureCount(address_, tempCount);
    temperature = Zerotronic::getTemperature(temperatureRef_, tempCountRef_, tempCount);
    return ret;
}

Error ZerotronicDevice::getSigma(double& sigma) const
{
    if(semaphore_){
        xSemaphoreTake(semaphore_, portMAX_DELAY);
    }
    sigma = actualSigma_;
    if(semaphore_){
        xSemaphoreGive(semaphore_);
    }
    return lastError_;
}

Error ZerotronicDevice::getMean(double& mean) const
{
    if(semaphore_){
        xSemaphoreTake(semaphore_, portMAX_DELAY);
    }
    mean = actualMean_;
    if(semaphore_){
        xSemaphoreGive(semaphore_);
    }
    return lastError_;
}
