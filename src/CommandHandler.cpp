/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/
#include "CommandHandler.hpp"
#include "PhotoTaker.hpp"
#include "Zerotronic.hpp"
#include "ZerotronicDevices.hpp"
#include "UserLed.hpp"

CommandHandler::CommandHandler(HardwareSerial& serial) :
    serial_(serial), parser_(serial), lastCommand_(nullptr)
{
}

Error CommandHandler::handleParameter(Configuration::Parameter<int>& param, bool allowEmpty)
{
    const char* arg = parser_.getNextParameter();
    Error err = allowEmpty ? Error::SILENT : Error::INVALID_ARGUMENT;
    if(arg){
        if(std::string(arg) == "?"){
            serial_.printf("%s: %d\n", param.getName(), param.getValue());
            err = Error::SILENT;
        }else{
            err = param.setValue(arg);
        }
    }
    return err;
}

Error CommandHandler::handleParameter(Configuration::Parameter<double>& param, bool allowEmpty)
{
    const char* arg = parser_.getNextParameter();
    Error err = allowEmpty ? Error::SILENT : Error::INVALID_ARGUMENT;
    if(arg){
        if(std::string(arg) == "?"){
            serial_.printf("%s: %f\n", param.getName(), param.getValue());
            err = Error::SILENT;
        }else{
            err = param.setValue(arg);
        }
    }
    return err;
}

Error CommandHandler::handleClinoAcquisitions(bool allowEmpty)
{
    Error err = handleParameter(Configuration::getInstance()->getClinoAcquisitions(), allowEmpty);
    if(!err){
        ZerotronicDevices::getInstance()->setReadSamples(Configuration::getInstance()->getClinoAcquisitions().getValue());
    }
    return err;
}

Error CommandHandler::handleClinoFrequency(bool allowEmpty)
{
    Error err = handleParameter(Configuration::getInstance()->getClinoFrequency(), allowEmpty);
    if(!err){
        ZerotronicDevices::getInstance()->setReadFrequency(Configuration::getInstance()->getClinoFrequency().getValue());
    }
    return err;
}

Error CommandHandler::handleCommunicationTimeout()
{
    Error err = handleParameter(Configuration::getInstance()->getComTimeOut());
    if(!err){
        Zerotronic::getInstance()->setCommunicationTimeout(Configuration::getInstance()->getComTimeOut().getValue());
    }
    return err;
}

Error CommandHandler::handleParameters()
{
  if (parser_.hasNext())
  {
    Error err = handleParameter(Configuration::getInstance()->getPhotoAcquisitions());
    err |= handleParameter(Configuration::getInstance()->getPhotoInterval(), true);
    err |= handleParameter(Configuration::getInstance()->getFlashPower(), true);
    err |= handleParameter(Configuration::getInstance()->getPreLight(), true);
    err |= handleParameter(Configuration::getInstance()->getFlashDuration(), true);
    err |= handleClinoAcquisitions(true);
    err |= handleClinoFrequency(true);
    err |= handleParameter(Configuration::getInstance()->getSigmaLimit(), true);
    return err;
  }else{
    Serial.printf("PhotoAcquisitions: %d\n", Configuration::getInstance()->getPhotoAcquisitions().getValue());
    Serial.printf("PhotoInterval: %ds\n", Configuration::getInstance()->getPhotoInterval().getValue());
    Serial.printf("FlashPower: %d%%\n", Configuration::getInstance()->getFlashPower().getValue());
    Serial.printf("PreLight: %dms\n", Configuration::getInstance()->getPreLight().getValue());
    Serial.printf("FlashDuration: %dms\n", Configuration::getInstance()->getFlashDuration().getValue());
    Serial.printf("ClinoAcquisitions: %dms\n", Configuration::getInstance()->getClinoAcquisitions().getValue());
    Serial.printf("ClinoFrequency: %fHz\n", Configuration::getInstance()->getClinoFrequency().getValue());
    Serial.printf("SigmaLimit: %f\n", Configuration::getInstance()->getSigmaLimit().getValue());
  }
  return Error::NO_ERROR;
}

Error CommandHandler::handleEcho()
{
  const char* arg = parser_.getNextParameter();
  if(arg){
    std::string argStr(arg);
    if(argStr == "on"){
      parser_.setEchoEnabled(true);
    }else{
      parser_.setEchoEnabled(false);
    }
  }else{
    parser_.setEchoEnabled(!parser_.echoEnabled());
  }
  return Configuration::getInstance()->getSerialEcho().setValue(parser_.echoEnabled());
}

Error CommandHandler::handleTrigger()
{
  Error err = Error::INVALID_ARGUMENT;
  const char* arg = parser_.getNextParameter();
  if(arg){
    std::string argStr(arg);

    //Optionnal count and delay between shots
    int count = Configuration::getInstance()->getPhotoAcquisitions().getValue();
    int delaySeconds = Configuration::getInstance()->getPhotoInterval().getValue();
    arg = parser_.getNextParameter();
    if(arg){
      try{
          count = (std::stoi(arg));
      }catch (std::invalid_argument const& ex){
          return Error::INVALID_ARGUMENT;
      }
      catch (std::out_of_range const& ex){
          return Error::OUT_OF_RANGE;
      }
    }

    arg = parser_.getNextParameter();
    if(arg){
      try{
          delaySeconds = (std::stoi(arg));
      }catch (std::invalid_argument const& ex){
          return Error::INVALID_ARGUMENT;
      }
      catch (std::out_of_range const& ex){
          return Error::OUT_OF_RANGE;
      }
    }

    if(argStr == "full"){
      double sigma, threshold;
      err = PhotoTaker::full(sigma, threshold, count, delaySeconds);
      if(!err){
        //Print stats
        err = handleStats();
      }
    }else if(argStr == "photo"){
      err = PhotoTaker::photo(count, delaySeconds);
      serial_.printf("Ok\n");
    }
  }
  return err;
}

Error CommandHandler::handleSigma()
{
  std::size_t count = ZerotronicDevices::getInstance()->getDevices().size();
  if(count == 0)
    return Error::NO_DEVICES;

  ZerotronicDevices::DevicesVector& deviceCol = ZerotronicDevices::getInstance()->getDevices();
  for(ZerotronicDevices::DevicesVector::iterator it = deviceCol.begin();
      it !=deviceCol.end(); ++it){
      double sigma;
      Error err = (*it)->getSigma(sigma);
      if(err){
        serial_.printf("ERROR!");
      }else{
        serial_.printf("%5.4e", sigma);
      }
      serial_.print(' ');
  }
  serial_.println();
  return Error::SILENT;
}

Error CommandHandler::handleMean()
{
  std::size_t count = ZerotronicDevices::getInstance()->getDevices().size();
  if(count == 0)
    return Error::NO_DEVICES;

  ZerotronicDevices::DevicesVector& deviceCol = ZerotronicDevices::getInstance()->getDevices();
  for(ZerotronicDevices::DevicesVector::iterator it = deviceCol.begin();
      it !=deviceCol.end(); ++it){
      double mean;
      Error err = (*it)->getMean(mean);
      if(err){
        serial_.printf("ERROR!");
      }else{
        serial_.printf("%5.4e", mean);
      }
      serial_.print(' ');
  }
  serial_.println();
  return Error::SILENT;
}

Error CommandHandler::handleStats()
{
  std::size_t count = ZerotronicDevices::getInstance()->getDevices().size();
  if(count == 0)
    return Error::NO_DEVICES;

  ZerotronicDevices::DevicesVector& deviceCol = ZerotronicDevices::getInstance()->getDevices();
  for(ZerotronicDevices::DevicesVector::iterator it = deviceCol.begin();
      it !=deviceCol.end(); ++it){
      double sigma, mean;
      Error err = (*it)->getSigma(sigma);
      err |= (*it)->getMean(mean);
      if(err){
        serial_.printf("%d: Read error!\n", (*it)->getAddress());
      }else{
        serial_.printf("%d: Sigma: %5.4e Mean: %5.4e\n", (*it)->getAddress(), sigma, mean);
      }
  }
  serial_.printf("Ok\n");
  return Error::SILENT;
}

Error CommandHandler::handleProbe()
{
  int startAddress = 1;
  int endAddress = 10;
  const char* arg = parser_.getNextParameter();
  if(arg){
    startAddress = std::stoi(arg);
  }
  arg = parser_.getNextParameter();
  if(arg){
    endAddress = std::stoi(arg);
  }
  int devices = ZerotronicDevices::getInstance()->probeDevices(startAddress, endAddress);
  UserLed::getInstance()->setDevCount(devices);
  return handleDevices();
}

Error CommandHandler::handleDevices()
{
  Error err = Error::SILENT;
  std::size_t count = ZerotronicDevices::getInstance()->getDevices().size();
  if(count == 0)
    return Error::NO_DEVICES;
  if(parser_.echoEnabled()){
    if(count == 1){
      serial_.printf("Found 1 device:\n");
    }else{
      serial_.printf("Found %d devices:\n", count);
    }
  }
  ZerotronicDevices::DevicesVector& deviceCol = ZerotronicDevices::getInstance()->getDevices();
  for(ZerotronicDevices::DevicesVector::iterator it = deviceCol.begin();
      it !=deviceCol.end(); ++it){
      serial_.printf("%d : %s\n", (*it)->getAddress(), (*it)->getSerial().c_str());
  }
  if(!parser_.echoEnabled())
    serial_.println();
  return Error::SILENT;
}

Error CommandHandler::handleLED()
{
  Error err = Error::NO_ERROR;
  uint8_t rgb[] = {0,0,0};
  for(int i=0;i<3;++i){
    const char* arg = parser_.getNextParameter();
    if(arg){
      try{
          rgb[i] = (uint8_t)(std::stoi(arg));
      }catch (std::invalid_argument const& ex){
          return Error::INVALID_ARGUMENT;
      }
      catch (std::out_of_range const& ex){
          return Error::OUT_OF_RANGE;
      }
    }
  }
  UserLed::getInstance()->customColor(rgb[0], rgb[1], rgb[2]);
  return err;
}

Error CommandHandler::handleHelp()
{
  const char* arg = parser_.getNextParameter();
  std::map<std::string, Commands>::const_iterator search = callbacks_.end();
  if(arg){
    search = callbacks_.find(arg);
  }

  if((search != callbacks_.end()) && (search->second.help)){
    serial_.print(search->first.c_str());
    serial_.print(": ");
    serial_.println(search->second.help);
  }else{
    for(search=callbacks_.begin();search!=callbacks_.end();++search){
      if(search->second.help){
        serial_.print(search->first.c_str());
        serial_.print(": ");
        serial_.println(search->second.help);
      }
    }
  }
  return Error::SILENT;
}

Error CommandHandler::handleTemperature()
{
  int device = -1;
  const char* arg = parser_.getNextParameter();
  if(arg){
    try{
        int addr = std::stoi(arg);
        ZerotronicDevices::DevicesVector& deviceCol = ZerotronicDevices::getInstance()->getDevices();
        for(ZerotronicDevices::DevicesVector::iterator it = deviceCol.begin();
            it !=deviceCol.end(); ++it){
              if((*it)->getAddress() == addr){
                device = addr;
                break;
              }
        }
        if(addr != device){
          return Error::OUT_OF_RANGE;
        }
    }catch (std::invalid_argument const& ex){
        return Error::INVALID_ARGUMENT;
    }
    catch (std::out_of_range const& ex){
        return Error::OUT_OF_RANGE;
    }
  }
  ZerotronicDevices::DevicesVector& deviceCol = ZerotronicDevices::getInstance()->getDevices();
  for(ZerotronicDevices::DevicesVector::iterator it = deviceCol.begin();
      it !=deviceCol.end(); ++it){
        if((device == -1) || ((*it)->getAddress() == device)){
          double temp = 0.0;
          Error err = (*it)->readTemperature(temp);
          if(err){
            serial_.printf("%d : %s\n", (*it)->getAddress(), err.toString());
          }else{
            serial_.printf("%d : %f\n", (*it)->getAddress(), temp);
          }
        }
  }
  serial_.printf("Ok\n");
  return Error::SILENT;
}

void CommandHandler::begin()
{
    parser_.setEchoEnabled(Configuration::getInstance()->getSerialEcho().getValue());
}

void CommandHandler::loop()
{
  parser_.loop();
  bool emptyCmd = false;
   if(parser_.commandReceived(emptyCmd)){
    if(emptyCmd && lastCommand_){
      //Replay previous command
      parser_.revert();
    }else{
      if(parser_.echoEnabled()){
        serial_.println();
      }
    }
    const char* arg = parser_.getNextParameter();
    if(arg){
      //Make first argument case insensitive
      std::string argStr(arg);
      std::transform(argStr.begin(), argStr.end(), argStr.begin(), [](unsigned char c){ return std::tolower(c); });

      std::map<std::string, Commands>::const_iterator search = callbacks_.find(argStr);
      if(search != callbacks_.end()){
        Error err = search->second.callback();
        if(!err){
          serial_.printf("%s\n", err.toString());
        }else if(err != Error::SILENT){
          serial_.printf("Error [%s] : %s\n", search->first.c_str(), err.toString());
        }
        //Keep reference to previous command, if replayable
        if(search->second.canRepeat){
          lastCommand_ = &(search->second);
        }else{
          lastCommand_ = nullptr;
        }
      }else{
        //Command not found
        serial_.printf("Error [%s] : Bad command\n", arg);
      }
    }
    parser_.reset();
  }
}
