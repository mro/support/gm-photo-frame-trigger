/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/
#include "CommandParser.hpp"

CommandParser::CommandParser(HardwareSerial& serial) : 
    serial_(serial), cmdBuffer{}, curIndex_(0),
    readIndex_(0), cmdReceived_(false), echo_(false)
{
}

void CommandParser::loop()
{
    if(!cmdReceived_ && (serial_.available() > 0)){
        for(int i=0;i<serial_.available();++i){
            if(curIndex_ < (CMD_BUFFER_SIZE-1)){
                //Put char in the buffer
                char rcv;
                serial_.readBytes(&rcv, 1);
                //Filter to text, backspace and CMD_END
                if((rcv == 0x7F) || (rcv == 0x8)){
                    //Skip del and backspace
                    if(curIndex_ > 1){
                        --curIndex_;
                    }else{
                        curIndex_ = 0;
                    }
                }else if(rcv == CMD_END){
                    //End of command
                    cmdReceived_ = true;
                    readIndex_ = 0;
                    if(curIndex_ == 0){
                        //Empty command, preserve existing buffer
                        emptyCmd_ = true;
                    }else{
                        emptyCmd_ = false;
                        //Clears end of buffer
                        for(int j=curIndex_;j<CMD_BUFFER_SIZE;++j){
                            cmdBuffer[j] = CMD_END;
                        }
                    }
                    break;
                }else if((rcv >= ' ') && (rcv <= '~')){
                    cmdBuffer[curIndex_] = rcv;
                    //Valid ascii
                    curIndex_++;
                }else{
                    continue;
                }
                if(echo_){
                    serial_.print(rcv);
                }
            }
        }
    }
}

bool CommandParser::hasNext()
{
    //Nothing valid in buffer
    if(!cmdReceived_){
        return false;
    }
    //End reached
    if(cmdBuffer[readIndex_] == CMD_END){
        reset();
        return false;
    }

    //Skip all non-text
    while(true){
        char c = cmdBuffer[readIndex_];
        if(c == CMD_END){
            reset();
            return false;
        }
        //Text found, we are on next parameter
        if(isText(c)){
            break;
        }
        if(++readIndex_ >= CMD_BUFFER_SIZE){
            reset();
            return false;
        }   
    }
    return true;
}

const char* CommandParser::getNextParameter()
{
    //Nothing valid in buffer
    if(!hasNext() || emptyCmd_){
        return NULL;
    }    
    char * ret = &cmdBuffer[readIndex_];
    //Gets end
    int start = readIndex_;
    while(isText(cmdBuffer[start])){
        ++start;
    }
    cmdBuffer[start] = '\0';
    readIndex_ = start;
    return ret;
}

bool CommandParser::isText(char c)
{
    return ((c > ' ') && (c < 0x7F));
}