#include "Configuration.hpp"
#include <limits>
#include <SPIFFS.h>
#include "UserLed.hpp"


#ifndef JSON_BUFFER_SIZE
#define JSON_BUFFER_SIZE 512
#endif

#define PARAMETER_JSON_FILE "/parameters.json"

Configuration* Configuration::instance_ = nullptr;

template<>
Error Configuration::Parameter<int>::setValue(const std::string& value) {
    try{
        const int i{std::stoi(value)};
        return setValue(i);
    }catch (std::invalid_argument const& ex){
        return Error::INVALID_ARGUMENT;
    }
    catch (std::out_of_range const& ex){
        return Error::OUT_OF_RANGE;
    }
    return Error::NO_ERROR;
}

template<>
Error Configuration::Parameter<double>::setValue(const std::string& value) {
    try{
        const double i{std::stod(value)};
        return setValue(i);
    }catch (std::invalid_argument const& ex){
        return Error::INVALID_ARGUMENT;
    }
    catch (std::out_of_range const& ex){
        return Error::OUT_OF_RANGE;
    }
    return Error::NO_ERROR;
}

template<>
Error Configuration::Parameter<bool>::setValue(const std::string& value) {
    try{
        bool newValue = false;
        if((value == "true") || (value == "1") || (value == "True")){
            newValue = true;
        }
        return setValue(newValue);
    }catch (std::invalid_argument const& ex){
        return Error::INVALID_ARGUMENT;
    }
    catch (std::out_of_range const& ex){
        return Error::OUT_OF_RANGE;
    }
    return Error::NO_ERROR;
}

Configuration::Configuration() :
    photoAcquisitions_(*this, 1, 1, 200, "PhotoAcquisition"),
    photoInterval_(*this, 2, 0, 30, "PhotoInterval"),
    flashPower_(*this, 100, 1, 100, "FlashPower"),
    preLight_(*this, 10, 10, 500, "PreLight"),
    flashDuration_(*this, 50, 50, 2000, "FlashDuration"),
    clinoAcquisitions_(*this, 10, 1, 300, "ClinoAcquisitions"),
    clinoFrequency_(*this, 1, 1, 10, "ClinoFrequency"),
    sigmaLimit_(*this, 0.1, 0.0, std::numeric_limits<double>::max(), "SigmaLimit"),
    serialEcho_(*this, false, false, true, "SerialEcho"),
    comTimeout_(*this, 100, 0, 500, "CommTimeout"),
    lastModification_(0)
{
    instance_ = this;
}

void dumpJSON(JsonDocument& doc){
  serializeJsonPretty(doc, Serial);
  Serial.println();
}

void Configuration::begin()
{
    SPIFFS.begin(true);
    File configFile = SPIFFS.open(PARAMETER_JSON_FILE, "r");
    if(configFile){
        DynamicJsonDocument json(JSON_BUFFER_SIZE);
        if(deserializeJson(json, configFile) == DeserializationError::Ok) {
            // dumpJSON(json);
            photoAcquisitions_.loadFromJson(json);
            photoInterval_.loadFromJson(json);
            flashPower_.loadFromJson(json);
            preLight_.loadFromJson(json);
            flashDuration_.loadFromJson(json);
            clinoAcquisitions_.loadFromJson(json);
            clinoFrequency_.loadFromJson(json);
            sigmaLimit_.loadFromJson(json);
            serialEcho_.loadFromJson(json);
            comTimeout_.loadFromJson(json);
        }
    }
}

void Configuration::loop()
{
    if(lastModification_ > 0){
        if((millis()-lastModification_) > delayedSaveTime){
            saveToFlash();
            lastModification_ = 0;
        }
    }
}

Error Configuration::saveToFlash()
{
    UserLed::getInstance()->savingParameters();
    DynamicJsonDocument root(JSON_BUFFER_SIZE);
    photoAcquisitions_.toJSON(root);
    photoInterval_.toJSON(root);
    flashPower_.toJSON(root);
    preLight_.toJSON(root);
    flashDuration_.toJSON(root);
    clinoAcquisitions_.toJSON(root);
    clinoFrequency_.toJSON(root);
    sigmaLimit_.toJSON(root);
    serialEcho_.toJSON(root);
    comTimeout_.toJSON(root);
    File paramFile = SPIFFS.open(PARAMETER_JSON_FILE, "w");
    if(paramFile){
        serializeJson(root, paramFile);
        paramFile.close();
        UserLed::getInstance()->clear();
        return Error::NO_ERROR;
    }
    UserLed::getInstance()->error();
    return Error::SAVE_ERROR;
}
