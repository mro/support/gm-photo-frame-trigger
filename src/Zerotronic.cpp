/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/

#include "Zerotronic.hpp"
#include <stdio.h>

#define _USE_MATH_DEFINES
 
#include <cmath>

Zerotronic* Zerotronic::instance_ = nullptr;

Zerotronic::Zerotronic(HardwareSerial& serial) : 
    serial_(serial), comTimeout_(100), semaphore_(xSemaphoreCreateMutex())
{
    instance_ = this;
}

void Zerotronic::sendCommand(uint8_t address, uint8_t opCode, uint32_t data, uint8_t subAddress)
{
    /*
        how to set command string together:
        write gate time used as example:
            time calculation:  ((time [ms] / 2)-1) = value (dec) -> convert to hex
            data = 1000ms = 499 = 1F3 (hex);
        section:        explaination                                example string (part)    
        -------         ------------                                ---------------------           
        header:         4 or more '~'                               :   ~~~~
        rs 485 addr:    1 = 01(hex)                                 :       01
        subaddr:        not used , always 1                         :         1
        opcode:         writegate = opcode A(hex), see manual       :          A          
        data:           format depends on command, check manual                 
        "A003A0" is fixed, following of 1F3 timevalue(3 nibbles)    :           003A01F3
        Checksum:       Sum of Addr, subaddr, opcode and data   
                        0+1+1+A+0+0+3+A+0+1+F+3 = 2C(hex)                               2C
        complete commando                                           :   ~~~~011A003A01F32C + \r
        if checksum is invalid, no response occurs!
    */

    char cmdBuffer[preambleSize_ + 15];
    int i;
    for(i=0;i<preambleSize_;++i){
        cmdBuffer[i] = '~';
    }
    opCode &= 0xF;
    subAddress &= 0xF;
    sprintf(&cmdBuffer[i], "%02X%1X%1X", address, subAddress, opCode);
    i += 4;
    for(int j=0;j<4;++j){
        uint8_t dataByte = (data>> ((3-j) * 8) & 0xFF);
        sprintf(&cmdBuffer[i], "%02X", dataByte);
        i += 2;
    }
    sprintf(&cmdBuffer[i], "%02X", computeCheckSum(address, opCode, subAddress, data));
    i += 2;
    cmdBuffer[i] = '\r';
    xSemaphoreTake(semaphore_, portMAX_DELAY);
    //Flush RX buffer
    while(serial_.available() > 0){
        serial_.read();
    }
    serial_.write(cmdBuffer, sizeof(cmdBuffer)/sizeof(char));
    xSemaphoreGive(semaphore_);
#ifdef DEBUG_COMM
    cmdBuffer[i] = '\0';
    Serial.printf("Sending : %s\n", cmdBuffer);
#endif
}

Error Zerotronic::readCommand(uint8_t& address, uint8_t& opCode, uint8_t& subAddress, uint32_t& data)
{
    bool preambleFound = false;
    char c=0;
    xSemaphoreTake(semaphore_, portMAX_DELAY);
    while(true){
        Error r = readByte(c);
        if(r){
            //Timeout
            xSemaphoreGive(semaphore_);
            return r;
        }
        if(c == '~'){
            preambleFound = true;
        }else if(preambleFound){
            break;
        }
    }
    const int bufferLen = 15;
    char respBuffer[bufferLen];
    respBuffer[0] = c;
    for(int i=1;i<bufferLen;++i){
        Error r = readByte(c);
        if(r){
            //Timeout
            xSemaphoreGive(semaphore_);
            return r;
        }
        respBuffer[i] = c;
    }
    xSemaphoreGive(semaphore_);
    
    if(respBuffer[bufferLen-1] != '\r'){
        //Not expected
        return Error::BAD_PACKET;
    }
    respBuffer[bufferLen-1] = '\0';
#ifdef DEBUG_COMM    
    Serial.printf("Read : %s\n", respBuffer);
#endif    
    char* cPtr = respBuffer;
    if(!parseHex(cPtr, 2, address) || 
        !parseHex(cPtr, 1, subAddress) || 
        !parseHex(cPtr, 1, opCode) || 
        !parseHex(cPtr, 8, data)){        
        return Error::BAD_PACKET;
    }
    uint8_t chkSum = 0;
    if(!parseHex(cPtr, 2, chkSum)){
        return Error::BAD_PACKET;
    }
    if(chkSum != computeCheckSum(address, opCode, subAddress, data)){
        //Wrong checksum
        return Error::BAD_CHECKSUM;
    }
    return Error::NO_ERROR;
}

Error Zerotronic::readByte(char& byte)
{
    unsigned long start = millis();
    //Waits to get some bytes
    while(true){        
        if(serial_.available() > 0){
            break;
        }else{
            yield();
            if((millis() - start) > comTimeout_){
                return Error::TIMEOUT;
            }
        }
    }
    int ret = serial_.read();
    if(ret >= 0){
        byte = (char)ret;
        return Error::NO_ERROR;
    }
    return Error::TIMEOUT;
}

uint8_t Zerotronic::computeCheckSum(uint8_t address, uint8_t opCode, uint8_t subaddress, uint32_t data)
{
    int checkSum = 0;
    checkSum = (address >> 4) + (address & 0x0F) + subaddress + opCode;
    for(int j=0;j<4;++j){
        uint8_t dataByte = (data>> ((3-j) * 8) & 0xFF);
        checkSum += (dataByte >> 4) + (dataByte & 0xF);
    }
    return checkSum & 0xFF;
}

Error Zerotronic::readEprom(uint8_t address, uint16_t romAddr, uint8_t& data, uint8_t subAddress)
{
    uint32_t tmpData = (romAddr << 8);
    sendCommand(address, OPCode::ReadEeprom, tmpData, subAddress);
    uint8_t rAddr;
    Error ret = readCommand(rAddr, tmpData);
    data = (tmpData & 0xFF);
    return ret;
}

Error Zerotronic::readSerialNumber(uint8_t address, String& str, uint8_t subAddress)
{
    /**
        According to SDK:
        Serial number is stored as 4 consecutive bytes in the EEPROM (persistent storage) at address 0x0A in little en-
        dian format. To read the serial number the read EEPROM command must be used to read each byte of the serial 
        number. 

        The command is assembled as follows: 
        The read EEPROM command is selected by the opcode number 2. While the address is the parameter of the 
        command.  
    */
    Error ret;
    uint32_t serial = 0;
    for(int i=0;i<4;++i){
        //EEPROM address
        uint16_t romAddr = i + 0xA;
        uint8_t rData;
        ret = readEprom(address, romAddr, rData, subAddress);
        if(ret){
            return ret;
        }
        serial |= (rData << (i*8));
    }
    char yearCode = (serial/10000) + '@';
    str = yearCode;
    str += (serial % 10000);
    return ret;
}

Error Zerotronic::setGateTime(uint8_t address, uint16_t gateTime, uint8_t subAddress)
{
    uint32_t data = 0x003A0000; //3A prefix, needed according to doc.
    data |= (((gateTime/2)-1) & 0xFFF);
    sendCommand(address, OPCode::WriteGateTime, data, subAddress);
    return readCommand(address, data);
}

Error Zerotronic::readID(uint8_t address, uint16_t& id, uint16_t& firmwareVer, uint8_t subAddress)
{
    sendCommand(address, OPCode::ReadID, 0, subAddress);
    uint32_t data = 0;
    Error ret = readCommand(address, data);
    id = (data & 0xFFF);
    firmwareVer = (data >> 16) & 0x3FF;
    return ret;
}

Error Zerotronic::readTemperatureCount(uint8_t address, uint32_t& temperature, uint8_t subAddress)
{
    sendCommand(address, OPCode::ReadCountT, 0, subAddress);
    return readCommand(address, temperature);
}

Error Zerotronic::readReferenceTemperatureCelsius(uint8_t address, float& temperatureRef, uint8_t subAddress)
{
    Error ret;
    uint32_t refRaw = 0;
    for(int i=0;i<4;++i){
        //EEPROM address
        uint16_t romAddr = i + 0x48;
        uint8_t rData;
        ret = readEprom(address, romAddr, rData, subAddress);
        if(ret){
            return ret;
        }
        refRaw |= (rData << (i*8));
    }
    temperatureRef = *(float*)&refRaw;
    return ret;
}

Error Zerotronic::readReferenceTemperatureCount(uint8_t address, uint16_t& tempCountRef, uint8_t subAddress)
{
    Error ret;
    tempCountRef = 0;
    for(int i=0;i<2;++i){
        //EEPROM address
        uint16_t romAddr = i + 0x9E;
        uint8_t rData;
        ret = readEprom(address, romAddr, rData, subAddress);
        if(ret){
            return ret;
        }
        tempCountRef |= (rData << (i*8));
    }
    return ret;
}

Error Zerotronic::readAngle(uint8_t address, double& angleRad, uint8_t& seqNumber, uint8_t subAddress)
{
    sendCommand(address, OPCode::ReadAngle, 0, subAddress);
    uint32_t data;
    Error ret = readCommand(address, data);
    int32_t signedData =  ((data & 0xFFFFFFF) << 4);
    angleRad = (double)(signedData) / double(1<<28);
    seqNumber = (data >> 28) & 0xF;
    return ret;
}

double Zerotronic::getTemperature(float tempRef, uint16_t tempCountRef, uint32_t tempCount)
{
    //Magic formula
    return (double) tempRef <= 32000.0 ? (double) (tempCount - tempCountRef) / 379.0 + (double) tempRef : (double) (tempCount - tempCountRef) / 273.0 + (double)tempRef;
}

double Zerotronic::toArcSec(double radians)
{
    return radians * 180.0 / M_PI * 3600;
}

void Zerotronic::toDegrees(double radians, int32_t& degrees, int32_t& minutes, int32_t& seconds)
{
    int64_t fixed = (int64_t) toArcSec(radians);
    degrees = (int32_t)(fixed/3600);
    minutes = (int32_t)(fixed/60 - ((int64_t)degrees * 60));
    seconds = (int32_t) (fixed - (int64_t) (degrees * 3600 + minutes * 60));
}