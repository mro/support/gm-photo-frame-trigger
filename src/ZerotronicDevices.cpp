/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/

#include "ZerotronicDevices.hpp"
#include "UserLed.hpp"

ZerotronicDevices* ZerotronicDevices::instance_ = nullptr;

ZerotronicDevices::ZerotronicDevices(Zerotronic& comm) : 
    comm_(comm), readPeriod_(1000), readSamples_(10), 
    lastRead_(0), semaphore_(xSemaphoreCreateMutex()),
    state_(State::UNINITIALIZED), lastError_(Error::NO_ERROR)
{
    instance_ = this;
}

int ZerotronicDevices::probeDevices(int addrStart, int addrEnd)
{
    xSemaphoreTake(semaphore_, portMAX_DELAY );
    //Delete objects
    for(DevicesVector::iterator it=devices_.begin();it!=devices_.end();){
        if(*it)
            delete (*it);
        it = devices_.erase(it);
    }

    //Save the actual timeout
    unsigned long timeout = comm_.getCommunicationTimeout();
    comm_.setCommunicationTimeout(PROBE_TIMEOUT);

    int start = addrStart < addrEnd ? addrStart : addrEnd;
    int end = addrEnd > addrStart ? addrEnd : addrStart;
    //Try to get ID of devices
    for(int i=start; i<=end; ++i){
        uint16_t id, firmwareVer;
        Error ret = comm_.readID(i, id, firmwareVer);
        if(!ret){
            //Read OK
            ZerotronicDevice* dev = new ZerotronicDevice(i, comm_);
            dev->setId(id);
            dev->setFirmwareVersion(firmwareVer);
            String serial;
            ret = comm_.readSerialNumber(i, serial);
            if(ret){
                //Error occured  
                delete dev;
                continue;
            }
            dev->setSerial(serial);
            dev->setNbSamples(readSamples_);
            //Add device to collection
            devices_.push_back(dev);
        }
    }

    //Restore the timeout
    comm_.setCommunicationTimeout(timeout);
    //We are now ready to read devices
    state_ = State::READ;
    xSemaphoreGive(semaphore_);
    return devices_.size();
}

Error ZerotronicDevices::readSensors()
{
    Error ret;
    const unsigned long now = millis();
    if((now-lastRead_) >= readPeriod_){
        xSemaphoreTake(semaphore_, portMAX_DELAY );  
            double maxSigma = 0.0;
            ret = devices_.empty() ? Error::NO_DEVICES : Error::NO_ERROR;
            for(DevicesVector::iterator it=devices_.begin(); it!=devices_.end();++it){
                double angle, sigma;
                Error readError = (*it)->readSensor(angle);
                readError |= (*it)->getSigma(sigma);                
                if(!readError && (sigma > maxSigma)){
                    maxSigma = sigma;
                }
                ret |= readError;
            }
            lastError_ = ret;
            if(!lastError_){
                maxSigma_ = maxSigma;
                UserLed::getInstance()->clearError();
            }else{
                if(lastError_ == Error::OUT_OF_RANGE){
                    UserLed::getInstance()->warning();
                }else{
                    UserLed::getInstance()->error();
                }
                maxSigma_ = 0.0;
            }
        xSemaphoreGive(semaphore_);
        lastRead_ = millis();
    }
    return lastError_;
}

void ZerotronicDevices::loop()
{
    switch(state_){
        case State::UNINITIALIZED:
        case State::READY:
            break;
        case State::READ:
            readSensors();
    }
    yield();
}

Error ZerotronicDevices::setReadSamples(unsigned int sampleCount)
{
    Error ret;
    xSemaphoreTake(semaphore_, portMAX_DELAY );
    for(DevicesVector::iterator it=devices_.begin(); it!=devices_.end();++it){
        ret = (*it)->setNbSamples(sampleCount);
        if(ret){
            break;
        }
    }
    xSemaphoreGive(semaphore_);
    return ret;
}

Error ZerotronicDevices::setReadPeriod(unsigned int ms)
{
    Error ret;
    xSemaphoreTake(semaphore_, portMAX_DELAY );
    for(DevicesVector::iterator it=devices_.begin(); it!=devices_.end();++it){
        ret = (*it)->setGateTime(ms);
        if(ret){
            break;
        }
    }
    if(!ret){
        readPeriod_ = ms;
    }
    xSemaphoreGive(semaphore_);
    return ret;
}

Error ZerotronicDevices::getMaximumSigma(double& sigma)
{
    Error ret;
    xSemaphoreTake(semaphore_, portMAX_DELAY);
    sigma = maxSigma_;
    ret = lastError_;
    xSemaphoreGive(semaphore_);
    return ret;
}

Error ZerotronicDevices::getMean(double& mean)
{
    Error ret;
    mean = 0.0;
    xSemaphoreTake(semaphore_, portMAX_DELAY);
    ret = lastError_;
    for(DevicesVector::iterator it=devices_.begin(); it!=devices_.end();++it){
        double meanDev;
        (*it)->getMean(meanDev);
        mean += meanDev;
    }
    mean /= (double)devices_.size();
    xSemaphoreGive(semaphore_);
    return ret;
}