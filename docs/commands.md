# Photo frame trigger devices commands reference

## Introduction
The Photo frame trigger device is based on an ESP32S3 microcontroller. It acquires several inclinometers devices, control flash and camera trigger and compute some statistics of inclinometers measurements.

### Connecting the device
In order to connect a computer to the device, a micro-usb cable needs to be connected to the UART port of the board (blue connector on the picture below).
![ESP connection](pics/eps-con.jpg)

On the computer side, a serial port will be enumerated as “Silicon Labs CP210x”.

Communication parameters are:
-	Baud  rate : 115200
-	Data bits : 8
-	Stop bits : 1
-	Parity : None

### Command format
A command (i.e. from the computer to the device) is always terminated with a line feed (known as LF, 0xA or \n).  
It starts with the command name and some -optional- parameters depending on the command type.  
Parameters are separated with one (or more) spaces (0x20 character).  
Commands are not case-sensitive (the device convert all received valid characters to lowercase).  
For example, the trigger command expects to have full or photo as argument.  
The resulting command may be:
`Trigger full\n`  
The command: <code>trigger&nbsp;&nbsp;&nbsp;full&nbsp;&nbsp;\n</code>
is also a valid command (extra spaces are ignored, case is not important).  
Some commands can be repeated by just sending a line feed character.

### Commands response
Response of a valid command depends on the command type.  
If a command is unknown or command execution fails. The device will respond with this format:  
`Error [CMD] : ERROR_MESSAGE\n`  
Where *CMD* is the received command, and *ERROR_MESSAGE* is an user displayable error message.

Here is the list of possible error text:

|Error text	        |Reasons                                                                        |
|-------------------|:------------------------------------------------------------------------------|
|No devices	        |No devices were found during probe sequence.                                   |
|Bad packet	        |An invalid packet was received during communication with inclinometers.        |
|Timeout            |A timeout occurred during communication with inclinometers.                    |
|Bad checksum	    |Inclinometer response checksum computation failed.                             |
|Out of range	    |Inclinometer measurement is out of range. Parameter setting is out of range.   |
|Save error	        |Storing of parameters to flash failed.                                         |
|Invalid argument   |Invalid argument (parameters) received.                                        |
|Unstable           |System is unstable (sigma over threshold).                                     |

## Commands list
Here is the list of commands supported by the device.  
This document concerns the first version of the firmware. More commands maybe added in a new revision of the firmware.

### Settings commands
Some commands are used to configure the device.  
The same command name is used to set or get the setting value.  
Sending a `?` will respond the parameter name followed by the actual value.  
For example, sending `clinoacquisitions ?\n` will answer:  
`ClinoAcquisition 5\n`  
Meaning the *ClinoAcquisition* setting have an actual value of *5*.  
Settings commands can return an Out of range error if the value is not within specified range.  

Here is the list of valid settings:
|Name               |Type   | Minimum   | Maximum   | Unit  | Description                                               |
|------------------:|------:|----------:|----------:|------:|----------------------------------------------------------:|
|PhotoAcquisitions  |Integer|1          |50         |Count  |Number of photos to be taken.                              |
|PhotoInterval      |Integer|0          |30         |Seconds|Interval between photo captures.                           |
|FlashPower         |Integer|1          |100        |%      |Intensity of flash power.                                  |
|PreLight           |Integer|10         |50         |ms     |Time to pre-light before capture.                          |
|FlashDuration      |Integer|50         |2000       |ms     |Duration of the flash.                                     |
|ClinoAcquisitions  |Integer|1          |300        |Count  |Number of acquisitions in buffer.                          |
|ClinoFrequency     |Double |1          |10         |Hz     |Acquisition rate.                                          |
|SigmaLimit         |Double |0          |--         |rad    |Trigger full maximum angle sigma.                          |
|CommTimeout        |Integer|0          |500        |ms     |Clinometers communication timeout.                         |

### Other commands
#### devices
The `devices` command retrieves the list of devices found on the bus. It doesn’t need any arguments.

If the echo is activated, the response will give a line with the number of devices found followed by the list of devices (*ADDRESS : SERIAL*).  
For example:
```
Found 2 devices:\n
3 : S230\n
4 : S238\n
```
Which means two devices at address *3* and *4* with serial numbers *S230* and *S238* were found.  
If only one device is found, the first line will be `Found 1 device\n`.  
If echo is disabled, response will be:
```
3 : S230\n
4 : S238\n
\n
```

If no devices is found, an `Error [devices] : No devices\n` error will be returned.

### echo
The `echo` command enables or disables the serial echo on the serial line.  
If echo is active, all characters received on the serial line will be sent back to the computer.  
This command can be used without parameters. In this case, the echo mode is toggled.  
If a `on` parameter is provided to the command, the echo will be activated. Other parameter value will disable the echo.  
This command always answered `Ok\n`.  
For example, let's consider this sequence:
```
> echo on\n
< Ok\n
```
Echo is enabled
```
> echo\n
< Ok\n
```
Echo is disabled (was enabled before).
```
> echo\n
< Ok\n
```
Echo is enabled (was disabled before).
```
> echo 123\n
< Ok\n
```
Echo is disabled (parameter is not on).

### help
The help command displays all available commands supported by the device.  
Each command is followed by a short command description.

The `?` is an alias of the help command.

### led

The `led` command can be used to set the onboard RGB led to an arbitrary value.  
It may be followed by up to three parameters (0 to 255) defining the red, green and blue intensity.  
If corresponding parameters are not provided, it defaults to 0.  
This command always answered `Ok\n`.  
For example:
```
> led 255 255 255\n
< Ok\n
```
Led is fully on
```
> led 255\n
< Ok\n
```
Led is red (green and blue are 0).
```
> led 50 0 50\n
< Ok\n
```
Led is purple (red and blue is 50, green is 0)
```
> led\n
< Ok\n
```
Led is off (all colours to 0).

### mean
The mean command retrieves actual average value of sensors. Average is computed over all *ClinoAcquisitions* samples.  
A correct answer example is: `-1.2415e-01 -1.8012e-02\n`  
In this example, two devices were detected by the system. The one with the lower address is sent first (with a mean of -1.2415e-01 radians) and the one with next address is sent then.  
If no devices are detected, the response will be: `Error [mean] : No devices\n`  
If one of the sensor reading is faulty, the value corresponding to the sensor will be replaced by *ERROR!*, like in this example: `ERROR! -1.8570e-02\n`

### sigma
The sigma command retrieves actual standard deviation value of sensors. Standard deviation is computed over all *ClinoAcquisitions* samples.  
A correct answer example is: `7.2909e-04 4.1442e-04\n`
In this example, two devices were detected by the system. The one with the lower address is sent first (with a sigma of 7.2909e-04 radians) and the one with next address is sent then.  
If no devices are detected, the response will be: `Error [sigma] : No devices\n`  
If one of the sensor reading is faulty, the value corresponding to the sensor will be replaced by *ERROR!*, like in this example: `ERROR! -4.1442e-04\n`

### parameters
The `parameters` command lists all settings and actual value of these settings.   At the end of the list a `OK\n` line is returned.  
Here is an example of a parameters command:
```
PhotoAcquisitions: 10\n
PhotoInterval: 10ms\n
FlashPower: 100%\n
PreLight: 300ms\n
FlashDuration: 50ms\n
ClinoAcquisitions: 5ms\n
ClinoFrequency: 10.000000Hz\n
SigmaLimit: 0.000010\n
Ok\n
```

### stats
The `stats` command is used to retrieve sigma and mean values for each sensor.  
For each detected sensor on the bus, it responds a line containing the sensor address, the sigma value and the average.  
Example of the command response:
```
3: Sigma: 1.0267e-03 Mean: -1.2400e-01\n
4: Sigma: 1.0978e-03 Mean: -1.7925e-02\n
Ok\n
```
In this example, two devices at addresses *3* and *4* will give their statistics.  
At the end of the list a `OK\n` line is returned.  

### temperature
The `temperature` command will read sensors internal temperature (&deg;C).  
If no parameter is provided with the command, the device will read all temperature for all sensors detected by the probing procedure.  
You can provide a sensor address as command parameter. In this case, only the concerned sensor will be queried.  
Please note, that reading temperature will pre-empt statistics communication with sensors. This command should not be used at high frequency to allow the MCU to read angle value from sensors.
```
3 : 25.325048\n
4 : 25.377147\n
Ok\n
```

In this example, two devices at addresses 3 and 4 will give their temperature.
If communication with one device fails, the error message is displayed in place of the temperature value.  
At the end of the list a `OK\n` line is returned.  

### trigger
The trigger command is used to start a photo capture sequence.  
The command can accept two parameter values:
-	`photo`:
    This parameter will start a photo capture sequence without checking sensors stability.  
    The capture sequence is the following:
    1.	Write the flash intensity to the corresponding pin (square PWM at 1KHz)
    2.	Write the flash trigger pin to 1
    3.	Wait for PreLight setting
    4.	Write the photo trigger pin to 1
    5.	Waits 100ms
    6.	Write photo trigger and flash trigger pins to 0
    7.	Waits for PhotoInterval setting (seconds)
    8.	Repeat step 1. until PhotoAcquisitions are done
    
    The device will respond `OK\n` at the end of the sequence.  
-	`full`:
    This parameter will start a photo capture sequence if all sensors sigma are below the SigmaLimit parameter. If this limit is not reached, the sequence will be started as for the photo parameter.  
    You can pass two more arguments to the command.  
    The first one is the number of photos to be taken (it overrides the PhotoAcquisitions setting).  
    The second if the delay between each photo (it overrides the PhotoInterval setting) in seconds.  
    If the system is not stable, the device will respond with `Error [trigger] : Unstable\n`.   
    The same response of the `stat` command is output in this mode (if the device is stable).  
 

Executing this command is blocking. You can’t send other commands until the complete sequence is executed.
