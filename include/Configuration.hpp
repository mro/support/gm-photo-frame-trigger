/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-22T15:08:44
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/

#ifndef _CONFIGURATION_HPP__
#define _CONFIGURATION_HPP__

#include <stdexcept>
#include <string>
#include <ArduinoJson.h>
#include "Errors.hpp"

class Configuration{
public:

    template <typename T>
    class Parameter{
    public:
        
        T getValue() const { return value; }

        Error setValue(T newValue) {
            if(inRange(newValue)){
                value = newValue;
                cfg.lastModification_ = millis();
                return Error::NO_ERROR;
            }
            return Error::OUT_OF_RANGE;
        }

        Error setValue(const std::string& value){ return Error::INVALID_ARGUMENT; };

        const char* getName() const { return name; }

    private:
        Parameter(Configuration& cfg, T defValue, const T min, const T max, const char* name) : 
            cfg(cfg), value(defValue), defaultValue(defValue), minimumValue(min), maximumValue(max), name(name)
        {}

        void loadFromJson(const JsonDocument& doc){
            T newVal = defaultValue;
            if(doc.containsKey(name)){
                newVal = doc[name].as<T>();
                if(!inRange(newVal)){
                    //Fallback to default value if not in range
                    newVal = defaultValue;
                }
            }
            value = newVal;
        }

        void toJSON(JsonDocument& doc){
            doc[name] = value;
        }

        Configuration& cfg;
        T value;
        const T defaultValue;
        const T minimumValue;
        const T maximumValue;
        const char* name;

        inline bool inRange(T newValue) const { return ((newValue >= minimumValue) && (newValue <= maximumValue)); }

        template<bool>
        inline bool inRange(bool newValue) const { return true; }

        template<char*>
        inline bool inRange(char* newValue) const { return true; }

        friend Configuration;
    };
    
    Configuration();
    virtual ~Configuration() = default;
    void begin();
    void loop();

    Error saveToFlash();

    inline Parameter<int>& getPhotoAcquisitions() { return photoAcquisitions_; }
    inline Parameter<int>& getPhotoInterval() { return photoInterval_; }
    inline Parameter<int>& getFlashPower() { return flashPower_; }
    inline Parameter<int>& getPreLight() { return preLight_; }
    inline Parameter<int>& getFlashDuration() { return flashDuration_; }
    inline Parameter<int>& getClinoAcquisitions() { return clinoAcquisitions_; }
    inline Parameter<double>& getClinoFrequency() { return clinoFrequency_; }
    inline Parameter<double>& getSigmaLimit() { return sigmaLimit_; }
    inline Parameter<bool>& getSerialEcho() { return serialEcho_; }
    inline Parameter<int>& getComTimeOut(){ return comTimeout_; }
    
    static inline Configuration* getInstance() { return instance_; };

private:
    Parameter<int> photoAcquisitions_;
    Parameter<int> photoInterval_;
    Parameter<int> flashPower_;
    Parameter<int> preLight_;
    Parameter<int> flashDuration_;
    Parameter<int> clinoAcquisitions_;
    Parameter<double> clinoFrequency_;
    Parameter<double> sigmaLimit_;
    Parameter<bool> serialEcho_;
    Parameter<int> comTimeout_;
    unsigned long lastModification_;
    static const unsigned long delayedSaveTime = 500;
    static Configuration* instance_;
};

template<> Error Configuration::Parameter<int>::setValue(const std::string& value);
template<> Error Configuration::Parameter<double>::setValue(const std::string& value);
template<> Error Configuration::Parameter<bool>::setValue(const std::string& value);

#endif
