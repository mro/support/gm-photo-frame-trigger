#ifndef _ERRORS_H__
#define _ERRORS_H__

/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/


/**
 * Error definitions
*/
struct Error {
    enum ErrorCode {NO_ERROR=0, NO_DEVICES, BAD_PACKET, TIMEOUT, BAD_CHECKSUM, OVERLOAD, OUT_OF_RANGE, SAVE_ERROR, INVALID_ARGUMENT, SILENT, UNSTABLE};

    ErrorCode code_;

    Error() : code_(ErrorCode::NO_ERROR){}
    Error(ErrorCode code) : code_(code){}
    Error(const Error& e) : code_(e.code_){}

    const char* toString(){
        return toString(code_);
    }

    static const char* toString(ErrorCode code){
        switch(code){
            case ErrorCode::NO_ERROR:
                return "Ok";
            case ErrorCode::NO_DEVICES:
                return "No devices";
            case ErrorCode::BAD_PACKET:
                return "Bad packet";
            case ErrorCode::TIMEOUT:
                return "Timeout";
            case ErrorCode::BAD_CHECKSUM:
                return "Bad checksum";
            case ErrorCode::OUT_OF_RANGE:
                return "Out of range";
            case ErrorCode::SAVE_ERROR:
                return "Save error";
            case ErrorCode::INVALID_ARGUMENT:
                return "Invalid argument";
            case ErrorCode::SILENT:
                return "";
            case ErrorCode::UNSTABLE:
                return "Unstable";
        }
        return "Unknown error";
    }

    operator bool() const {
        return code_ != ErrorCode::NO_ERROR;
    }

    constexpr bool operator!=(const ErrorCode& e) {
        return code_ != e;
    }

    constexpr bool operator==(const ErrorCode& e) {
        return code_ == e;
    }
    
    Error &operator |=(const Error& b) {
        if(b.code_ != ErrorCode::NO_ERROR) {
            code_ = b.code_;
        }
        return *this;
    }
};

#endif