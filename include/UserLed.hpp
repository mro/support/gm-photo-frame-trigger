/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/
#ifndef _USER_LED_HPP__
#define _USER_LED_HPP__

#include <Arduino.h>
#include <FastLED.h>

/**
 * Simple class for managing user LED
*/
class UserLed {
public:
    /**
     * Constructor
    */
    UserLed();

    ~UserLed() = default;

    void begin();
    void loop();
    void startProbe();
    void clear();
    void setDevCount(int count);
    void sigmaOk();
    void sigmaNOk();
    void error();
    void warning();
    void clearError();
    void savingParameters();
    void customColor(uint8_t r, uint8_t g, uint8_t b);
    void preLight();
    void photoTrig();
    static inline UserLed* getInstance(){  return instance_; }
private:
    enum class State {CLEAR, PROBE_DEVICES, SHOW_DEV_COUNT, SIGMA_OK, SIGMA_NOK, ERROR, WARNING, SAVING_PARAMS, CUSTOM, PRE_LIGHT, FLASH};
    State state_;
    struct NewState{
        State state;
        uint32_t payload;
    };
    CRGB led_;
    SemaphoreHandle_t semaphore_;   //Semaphore to share ressource
    QueueHandle_t newStateQueue_;
    static UserLed* instance_;

    void setNewState(State newState, u_int32_t payload = 0);
    State getState();
};
#endif
