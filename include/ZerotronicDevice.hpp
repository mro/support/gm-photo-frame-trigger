/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/

#ifndef _ZEROTRONIC_DEVICE_H__
#define _ZEROTRONIC_DEVICE_H__

#include <Arduino.h>
#include "Errors.hpp"
#include "Zerotronic.hpp"

class ZerotronicDevice {
public:
    /**
     * Default constructor
     * @param address Device address on RS485 bus
     * @param comm Commmunicator object
    */
    ZerotronicDevice(uint8_t address, Zerotronic& comm);
    ~ZerotronicDevice() = default;
    ZerotronicDevice(const ZerotronicDevice&) = delete;

    /**
     * Gets device address
     * @return Device address on the RS485 bus
    */
    inline int getAddress() const { return address_; }

    /**
     * Gets device serial number
     * @return device serial number
    */
    inline const String& getSerial() const { return serial_; }

    /**
     * Sets the serial number
     * @param Serial number of the device
    */
    inline void setSerial(const String& serial) { serial_ = serial; }

    /**
     * Gets device ID
     * @return device ID
    */
    inline int16_t getId() const { return id_; }

    /**
     * Sets the ID of the device
     * @param id ID of the device
    */
    inline void setId(uint16_t id) { id_ = id; }

    /**
     * Gets device firmware version
     * @return device firmware version
    */
    inline int16_t getFirmwareVersion() const { return fwVersion_; }

    /**
     * Sets the firmware version
     * @param version Device firmware version
    */
    inline void setFirmwareVersion(uint16_t version) { fwVersion_ = version; }

    /**
     * Gets actual sigma
     * @param sigma actual standard deviation over all samples
     * @return Latest read error
    */
    Error getSigma(double& sigma) const;

    /**
     * Gets actual mean of measurements
     * @param mean actual mean over all samples
     * @return Latest read error
    */
    Error getMean(double& mean) const;

    /**
     * Reads sensor value and put it to buffer
     * @param angle Angle value
     * @return 0 on success
    */
    Error readSensor(double& angle);

    /**
     * Sets the number of samples for data acquisition
     * @return true if in range
    */   
    Error setNbSamples(int nbSamples);

    /**
     * Reads temperature from device
     * @param temperature Temperature output
     * @return 0 on success
    */
    Error readTemperature(double& temperature);

    /**
     * Sets the gate time
    */
    inline Error setGateTime(uint16_t gateTime) { return comm_.setGateTime(address_, gateTime); };
   
private:
    static const int MAX_BUFFER_SIZE = 300;     //Maximum size of buffer for acquisitons
    int address_;                               //Device address
    Zerotronic& comm_;                          //Communicator object
    SemaphoreHandle_t semaphore_;               //Semaphore to protect ressources
    int sampleIndex_;                           //Index of the current sample
    int sampleCount_;                           //Number of samples in the buffer
    int nbSamples_;                             //Number of samples for statistics computation    
    double actualSigma_;                        //Actual sigma of measurement
    double actualMean_;                         //Actual mean of measurement
    double samples_[MAX_BUFFER_SIZE];           //Samples buffer
    String serial_;                             //Device serial number
    uint16_t id_;                               //Device ID
    uint16_t fwVersion_;                        //Device firmware version
    uint16_t tempCountRef_;                     //Temperatrure reference count
    float temperatureRef_;                      //Temperature reference
    uint8_t prevSeqNumber_;                     //Previous sequence number
    Error lastError_;                           //Last read error
    void computeStatistics();                   //Computes the sigma and mean
};

#endif