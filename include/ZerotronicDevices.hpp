/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/

#ifndef _ZEROTRONIC_DEVICES__
#define _ZEROTRONIC_DEVICES__

#include "Zerotronic.hpp"
#include "ZerotronicDevice.hpp"
#include <vector>
#include <Errors.hpp>

class ZerotronicDevices {
public:
    /**
     * Constructor
     * @param comm Communicator object4
    */
    ZerotronicDevices(Zerotronic& comm);
    virtual ~ZerotronicDevices() = default;

    /**
     * Gets the communicator class
     * @return Reference to the communicator
    */
    inline Zerotronic& getCommunicator() { return comm_; }
    
    typedef std::vector<ZerotronicDevice*> DevicesVector;

    /**
     * Gets devices list
    */
    inline DevicesVector& getDevices() { return devices_; }

    /**
     * Probe devices and add ZerotronicDevice to collection 
     * @param addrStart Start address
     * @param addrEnd End address
    */
    int probeDevices(int addrStart = 1, int addrEnd = 10);

    /**
     * Acquisition loop
    */
    void loop();

    /**
     * Sets number of samples to read
     * @param sampleCount Number of samples to reads
     * @return Error if out-of-range
    */
    Error setReadSamples(unsigned int sampleCount);

    /**
     * Gets number of samples to read
    */
    inline int getReadSamples() const { return readSamples_; }

    /**
     * Sets read period of sensors
    */
    Error setReadPeriod(unsigned int ms);

    /**
     * Gets read period
     * @return read period in ms
    */
    inline int getReadPeriod() const { return readPeriod_; }

    /**
     * Helper to set the read frequency
    */
    inline Error setReadFrequency(double freq) { return setReadPeriod((int)(1000.0/freq)); }

    /**
     * Gets the read frequency
    */
    inline double getReadFrequency() const { return (1000.0/readPeriod_); }

    /**
     * Gets the maximum sigma of latest measurement
    */
    Error getMaximumSigma(double& sigma);

    /**
     * Gets the mean of latest measurement
    */
    Error getMean(double& mean);


    static inline ZerotronicDevices* getInstance(){ return instance_; }

private:
    const unsigned long PROBE_TIMEOUT = 100;
    ZerotronicDevices(const ZerotronicDevices&) = delete;
    Zerotronic& comm_;              //Reference to the communicator object
    DevicesVector devices_;         //Detected devices collection
    unsigned long readPeriod_;      //Read period
    unsigned int readSamples_;      //Number of samples to compute sigma
    unsigned long lastRead_;        //Last read time
    SemaphoreHandle_t semaphore_;   //Semaphore to protect ressources
    double maxSigma_;               //Maximum sigma
    enum class State {UNINITIALIZED, READY, READ};
    State state_;                   //State machine
    Error lastError_;               //Read error
    Error readSensors();            //Read sensors
    static ZerotronicDevices* instance_;
};

#endif