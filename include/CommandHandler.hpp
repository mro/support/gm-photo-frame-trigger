/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-22T15:08:44
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/
#ifndef _COMMANDS_HPP__
#define _COMMANDS_HPP__

#include <Arduino.h>
#include "Errors.hpp"
#include "CommandParser.hpp"
#include "Configuration.hpp"
#include "ZerotronicDevices.hpp"
#include <functional>
#include <map>

class CommandHandler {
public:
    CommandHandler(HardwareSerial& serial);
    virtual ~CommandHandler() = default;
    
    void begin();
    void loop();
private:
    Error handleClinoAcquisitions(bool allowEmpty=false);
    Error handleClinoFrequency(bool allowEmpty=false);
    Error handleCommunicationTimeout();
    Error handleParameters();
    Error handleEcho();
    Error handleTrigger();
    Error handleSigma();
    Error handleMean();
    Error handleStats();
    Error handleProbe();
    Error handleDevices();
    Error handleHelp();
    Error handleTemperature();
    Error handleLED();

    Error handleParameter(Configuration::Parameter<int>& param, bool allowEmpty=false);
    Error handleParameter(Configuration::Parameter<double>& param, bool allowEmpty=false);


    HardwareSerial& serial_;
    CommandParser parser_;
    typedef std::function<Error()> CallBackFunc;
    struct Commands {
        CallBackFunc callback;
        const char* help;
        const bool canRepeat;
        Commands(CallBackFunc callback, const char* help=nullptr, const bool canRepeat = false) :
            callback(callback), help(help), canRepeat(canRepeat){}
    };

    const std::map<std::string, Commands> callbacks_ = {
        {"help", {std::bind(&CommandHandler::handleHelp, this), "Show help."}},
        {"?", {std::bind(&CommandHandler::handleHelp, this)}},
        {"trigger", {std::bind(&CommandHandler::handleTrigger, this), "Trigger a photo capture sequence.", true}},
        {"sigma", {std::bind(&CommandHandler::handleSigma, this), "Gets maximum sigma for all clinometers.", true}},
        {"mean", {std::bind(&CommandHandler::handleMean, this), "Gets mean for all clinometers.", true}},
        {"stats", {std::bind(&CommandHandler::handleStats, this), "Gets clinometers sigma and mean.", true}},
        {"probe", {std::bind(&CommandHandler::handleProbe, this), "Search for clinometers on serial bus."}},
        {"temperature", {std::bind(&CommandHandler::handleTemperature, this), "Read temperature of sensors.", true}},
        {"photoacquisitions", {[this](){ return handleParameter(Configuration::getInstance()->getPhotoAcquisitions()); }, "Sets number of photo acquisitions."}},
        {"photointerval", {[this](){ return handleParameter(Configuration::getInstance()->getPhotoInterval()); }, "Sets interval between photo acquisitions (ms)."}},
        {"flashpower", {[this](){ return handleParameter(Configuration::getInstance()->getFlashPower()); }, "Sets flash power (0-100%)."}},
        {"prelight", {[this](){ return handleParameter(Configuration::getInstance()->getPreLight()); }, "Sets pre-lighting period (ms)."}},
        {"flashduration", {[this](){ return handleParameter(Configuration::getInstance()->getFlashDuration()); }, "Sets flash duration (ms)."}},
        {"clinoacquisitions", {[this](){ return handleClinoAcquisitions(); }, "Sets number of clinometer acquisitions."}},
        {"clinofrequency", {[this](){ return handleClinoFrequency(); }, "Sets acquisition frequency of clinometers (Hz)."}},
        {"sigmalimit", {[this](){ return handleParameter(Configuration::getInstance()->getSigmaLimit()); }, "Sets max sigma limit to consider stable."}},
        {"parameters", {std::bind(&CommandHandler::handleParameters, this), "Sets all parameters in a row."}},
        {"echo", {std::bind(&CommandHandler::handleEcho, this), "Sets serial echo on/off."}},
        {"led", {std::bind(&CommandHandler::handleLED, this), "Sets RGB LED."}},
        {"timeout", {std::bind(&CommandHandler::handleCommunicationTimeout, this), "Sets communication timeout. (ms)"}},
        {"devices", {std::bind(&CommandHandler::handleDevices, this), "List discovered devices."}},
    };
    const Commands* lastCommand_;
};





#endif