#ifndef _PHOTOTAKER_HPP_
#define _PHOTOTAKER_HPP_

#include "Errors.hpp"

class PhotoTaker{
public:
    static void begin();
    static Error full(double& actualSigma, double& maxSigma, int count=1, int pauseSecs=0);
    static Error photo(int count=1, int pauseSecs=0);

private:
    PhotoTaker();
    virtual ~PhotoTaker() = default;

};

#endif
