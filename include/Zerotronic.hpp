/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/
#ifndef _ZEROTRONIC_HPP_
#define _ZEROTRONIC_HPP_

#include <Arduino.h>
#include "Errors.hpp"

class Zerotronic {
public:
    /**
     * Build a MultiTC object
     * @param serial Reference to serial port object
    */
    Zerotronic(HardwareSerial& serial);
    virtual ~Zerotronic() = default;

    /**
     * Sets the communication timeout
    */
    inline void setCommunicationTimeout(unsigned long timeout){ comTimeout_ = timeout; } 

    /**
     * Gets the communication timeout
    */
    inline unsigned long getCommunicationTimeout() { return (comTimeout_ > 0) ? comTimeout_ : 0; }

    /**
     * Send a command to MutiTC
     * @param address Device address
     * @param opCode Operation code
     * @param data Data to be written to device
     * @param subAddress Sub-address, not used (1 by default)
    */
    void sendCommand(uint8_t address, uint8_t opCode, uint32_t data = 0, uint8_t subAddress = 1);

    /**
     * Reads a command response from MultiTC
     * @param address Device address read from device
     * @param opCode Operation code operation code read from device
     * @param data Data to read from device
     * @param subAddress Sub-address, read from device
     * @return NO_ERROR On success
    */
    Error readCommand(uint8_t& address, uint8_t& opCode, uint8_t& subAddress, uint32_t& data);

    /**
     * Reads a command response from MultiTC
     * @param address Device address read from device
     * @param data Data to read from device
     * @return NO_ERROR On success
    */
    inline Error readCommand(uint8_t& address, uint32_t& data) { uint8_t opCode, subAddress; return readCommand(address, opCode, subAddress, data); }

    /**
     * Reads eeprom byte
     * @param address Device address read from device
     * @param romAddr Address to read from eeprom
     * @param data Data to read from device
     * @param subAddress Sub-address, not used
     * @return NO_ERROR On success
    */
    Error readEprom(uint8_t address, uint16_t romAddr, uint8_t& data, uint8_t subAddress = 1);

    /**
     * Gets device serial number
     * @param address Device address read from device
     * @param str Serial number string output
     * @param subAddress Sub-address, not used
     * @return NO_ERROR On success
    */
    Error readSerialNumber(uint8_t address, String& str, uint8_t subAddress = 1);

    /**
     * Sets the gate time
     * @param address Device address
     * @param gateTime Gate time in ms
     * @param subAddress Sub-address, not used
     * @return NO_ERROR On success 
    */
    Error setGateTime(uint8_t address, uint16_t gateTime, uint8_t subAddress = 1);

    /**
     * Reads device ID
     * @param address Device address
     * @param id ID read from system
     * @param firmwareVer Firmware version from system
     * @param subAddress Sub-address, not used
     * @return NO_ERROR On success 
    */
    Error readID(uint8_t address, uint16_t& id, uint16_t& firmwareVer, uint8_t subAddress = 1);

    /**
     * Reads the temperature ADC
     * @param address Device address
     * @param temperature temperature count from system
     * @param subAddress Sub-address, not used
     * @return NO_ERROR On success 
    */
    Error readTemperatureCount(uint8_t address, uint32_t& temperature, uint8_t subAddress = 1);

    /**
     * Read the temperature reference from EEPROM
     * @param address Device address
     * @param temperatureRef temperature reference from eeprom
     * @param subAddress Sub-address, not used
     * @return NO_ERROR On success
    */
    Error readReferenceTemperatureCelsius(uint8_t address, float& temperatureRef, uint8_t subAddress = 1);

    /**
     * Reads the reference temperature count from EEPROM
     * @param address Device address
     * @param tempCountRef temperature count reference from eeprom
     * @param subAddress Sub-address, not used
     * @return NO_ERROR On success
    */
    Error readReferenceTemperatureCount(uint8_t address, uint16_t& tempCountRef, uint8_t subAddress = 1);

    /**
     * Reads angle from sensor
     * @param address Device address
     * @param angleRad Angle in radians (-7/+7 -> Overrange)
     * @param seqNumber Sequence number
     * @param subAddress Sub-address, not used
     * @return NO_ERROR On success
    */
    Error readAngle(uint8_t address, double& angleRad, uint8_t& seqNumber, uint8_t subAddress = 1);

    /**
     * Gets temperature
     * @param tempRef Temperature reference read from EEPROM
     * @param tempCountRef Temperature count reference from EEPROM
     * @param tempCount Temperature count from device
     * @return Temperature in celsius
    */
    static double getTemperature(float tempRef, uint16_t tempCountRef, uint32_t tempCount);

    /**
     * Convert radians to degrees
    */
    static void toDegrees(double radians, int32_t& degrees, int32_t& minutes, int32_t& seconds);

    /**
     * Convert radians to arc seconds
    */
    static double toArcSec(double radians);

    /**
     * Gets singleton
    */
    static inline Zerotronic* getInstance() { return instance_; }

private:
    Zerotronic(Zerotronic& other) = delete;
    /**
     * Operation code, comes from reverse-engineering 
    */
    enum class OPCode {ReadID = 1, ReadEeprom = 2, ReadPort = 3, ReadActualPos = 4,
                    ReadCountS0 = 5, ReadCountS1 = 6, ReadPosition = 7,
                    ReadCountT = 8, WriteSynch = 9, WriteGateTime = 10 /* 0xA */,
                    WritePort = 11 /* 0xB */, WriteEeprom = 12 /* 0xC */,
                    ReadAngle = 13 /* 0xD */
    };
    inline void sendCommand(uint8_t address, OPCode opCode, uint32_t data = 0, uint8_t subAddress = 1) { sendCommand(address, (int8_t)opCode, data, subAddress); }
 

    /**
     * Parse hexadecimal string
     * @param str String to be parsed
     * @param nbDigits Number of digits to be parsed
     * @param data Parsed data output
     * @return true if conversion succeeded
    */
    template<typename T>
    static bool parseHex(char*& str, int nbDigits, T& data)
    {
        data = 0;
        for(int i=0;i<nbDigits;++i){
            char c = *(str++);
            uint8_t val = 0;
            if(c == '\0'){
                --str;
                return false;
            }else if(c >= '0' && c <= '9'){
                val = c - '0';
            }else if(c >= 'A' && c <= 'F'){
                val = (c - 'A') + 10;
            }else if(c >= 'a' && c <= 'f'){
                val = (c - 'a') + 10;
            }else{
                return false;
            }
            data |= (val << ((nbDigits-i-1)*4));
        }
        return true;
    }

    /**
     * Reads a byte from serial port within specified timeout
    */
    Error readByte(char& byte);
    static uint8_t computeCheckSum(uint8_t address, uint8_t opCode, uint8_t subaddress, uint32_t data);
    HardwareSerial& serial_;        //Reference to the serial port
    unsigned long comTimeout_;      //Communication timeout
    SemaphoreHandle_t semaphore_;   //Semaphore to share ressource
    const int preambleSize_ = 6;    //Size of the preamble (can be lowered to 4)
    static Zerotronic* instance_;   //Singleton
};

#endif