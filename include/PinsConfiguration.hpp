#ifndef _PIN_CONFIGURATION_HPP__
#define _PIN_CONFIGURATION_HPP__

//Second UART pin definition
#ifndef UART2_RX
#define UART2_RX 4
#endif

#ifndef UART2_TX
#define UART2_TX 5
#endif

//RGB LED, can be on pin 38 on new boards
#ifndef RGB_LED_PIN
#define RGB_LED_PIN 48
#endif

#ifndef PHOTO_TRIG_PIN
#define PHOTO_TRIG_PIN 14   //Photo trigger pin
#endif

#ifndef FLASH_TRIG_PIN
#define FLASH_TRIG_PIN 13   //Flash trigger pin
#endif

#ifndef FLASH_INT_PIN
#define FLASH_INT_PIN 12    //Flash intensity
#endif 

#endif