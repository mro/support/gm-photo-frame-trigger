/*
** Copyright (C) 2023 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2023-09-21T15:19:48+02:00
**     Author: Mathieu Donze <mdonze> <mathieu.donze@cern.ch>
**
*/

#ifndef _COMMAND_PARSER_HXX__
#define _COMMAND_PARSER_HXX__

#include <Arduino.h>
#include "Errors.hpp"

/**
 * A very simple command parser
*/
class CommandParser {
public:
    CommandParser(HardwareSerial& serial);
    virtual ~CommandParser() = default;
    
    void loop();
    /**
     * Gets if a command is received
     * @return true if a command is ready to be processed
    */
    inline bool commandReceived(bool& empty) { empty = emptyCmd_; return cmdReceived_; }

    /*
        Gets next parameter
        @return Pointer to next parameter string or NULL if no more parameters
    */
    const char* getNextParameter();

    /**
     * Gets if next parameter is available
    */
    bool hasNext();

    /**
     * Resets command reception
    */
    inline void reset() { cmdReceived_ = false; curIndex_ = 0;}

    /*
    *   Revert buffer to previous command
    */                
    inline void revert() { readIndex_ = 0; emptyCmd_ = false; }


    /**
     * Gets if serial is enabled
    */
    inline bool echoEnabled() { return echo_; }

    /**
     * Sets echo enabled
    */
    inline void setEchoEnabled(bool enabled) { echo_ = enabled; }

private:
    static const int CMD_BUFFER_SIZE = 200;     //Command buffer size
    static const char CMD_END = '\n';           //Command end of line
    HardwareSerial& serial_;                    //Reference to serial port
    char cmdBuffer[CMD_BUFFER_SIZE];            //Command buffer
    int curIndex_;                              //Actual buffer index
    int readIndex_;                             //Read index
    bool cmdReceived_;                          //A valid command is received
    bool emptyCmd_;                             //An empty command is received
    bool echo_;                                 //Serial echo enabled
    /**
     * Checks if character is text
    */
    static bool isText(char c);
};

#endif